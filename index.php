<?php
include('db.php');

$per_page = 4; 
$select_table = "SELECT * FROM pagination";
$variable = mysql_query($select_table);
$count = mysql_num_rows($variable);
$pages = ceil($count/$per_page)

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Qjuery pagination with loading effect using PHP and MySql</title>

<script type="text/javascript" src="jquery.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){

			function Display_Load()
			{
			    $("#load").fadeIn(1000,0);
				$("#load").html("<img src='load.gif' />");
			}

			function Hide_Load()
			{
				$("#load").fadeOut('slow');
			};

			$("#paginate li:first").css({'color' : '#FF0084'}).css({'border' : 'none'});

			Display_Load();

			$("#content").load("pagination.php?page=1", Hide_Load());

			$("#paginate li").click(function() {
				Display_Load();
				$("#paginate li")
				.css({'border' : 'solid #193d81 1px'})
				.css({'color' : '#0063DC'});
				$(this)
				.css({'color' : '#FF0084'})
				.css({'border' : 'none'});
				var pageNum = this.id;
				$("#content").load("pagination.php?page=" + pageNum, Hide_Load());
			});
		});
	</script>
    
    
<style type="text/css">
body { 
margin: 0; 
padding: 0; 
font-family:Tahoma, Geneva, sans-serif; 
font-size:18px;
 }

#content{ 
margin:0 auto; 
border:0px green dashed; 
width:800px; 
min-height:150px; 
margin-top:100px;
 }
#load { 
width:30px;
padding-top:50px;
border:0px green dashed;
margin:0 auto;
}
#paginate
{
text-align:center;
border:0px green solid;
width:500px;
margin:0 auto;
}
.link{
width:800px; 
margin:0 auto; 
border:0px green solid;
}

li{	
list-style: none; 
float: left;
margin-right: 16px; 
padding:5px; 
border:solid 1px #193d81;
color:#0063DC; 
}
li:hover
{ 
color:#FF0084; 
cursor: pointer; 
}
</style>


</head>

<body>

	<div id="content"></div>

	<div class="link" align="center">
		<ul id="paginate">
			<?php
			//Show page links
			for($i=1; $i<=$pages; $i++)
			{
				echo '<li id="'.$i.'">'.$i.'</li>';
			}
			?>
		</ul>	
	</div>

    <div style="clear:both"> </div>
	<div id="load" align="center"></div>

</body>
</html>